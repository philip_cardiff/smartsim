# SmartSim Project #

The smartSim toolbox consists of libraries and utilities to create supervised and physics-informed machine learning models for OpenFOAM cases, as well as example tutorial cases.

## Who maintains the smartSim toolbox? ###

Emad Tandis, University College Dublin, emad.tandis@ucd.ie
Philip Cardiff, University College Dublin, philip.cardiff@ucd.ie

## What is the smartSim toolbox used for? ###

This repository includes codes to:
- Generate data as training data-set for supervised and physics-informed ML models;
- Create and train supervised and physics-informed machine learning models based on OpenFOAM inputs;
- Use pre-trained supervised and physics-informed machine learning models as initialisers in OpenFOAM solvers.

## How to install the smartSim toolbox? ###

The smartSim toolbox uses the TensorFlow C API which is linked with OpenFOAM. This is somewhat tedious to setup from scratch, so a Docker image is provided for ease of installation which contains all the required dependencies and works out-of-the-box.

### Installing the Docker image

There are two ways to install this image:

#### Method 1: ####

Download the pre-created public image from https://hub.docker.com using the following shell command:

    docker pull emad64/smartsim4foam:v1.0


#### Method 2: ####

Build an image using the Dockerfile included within the project. Run the following command in the parent directory of the smartSim repository to build the image (note that this may take a few hours):

    docker build -t emad64/smartsim4foam:v1.0 .


### Creating and running a Docker container of the image

Once you have downloaded or built the `ml_openfoam_ubuntu18` image, you can create a container from this image using the command:

    docker run -ti --rm  --name container_smartsim4foam emad64/smartsim4foam:v1.0


You can exit and stop a running container with:

    exit


## How to run the tutorials

Read [tutorials/README.md](./tutorials/README.md) for steps on running the tutorials.


## What is the structure of the smartSim toolbox?

The smartSim toolbox is composed of the following components:

- src : this is the source code directory containing utilities, classes and scripts for generating data, creating and training machine learning models, and making predictions with machine learning models from within python and OpenFOAM code;

- tutorials : this directory contains example tutorial cases showing how the procedures are used.


