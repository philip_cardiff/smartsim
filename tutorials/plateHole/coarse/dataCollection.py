import multiprocessing as mp
import sys,os
import numpy as np
from os import path
import subprocess
import itertools
import re
import shutil
from tempfile import mkstemp
from distutils.dir_util import mkpath



def sed(pattern, replace, source, dest=None, count=0):
    """Reads a source file and writes the destination file.

    In each line, replaces pattern with replace.

    Args:
        pattern (str): pattern to match (can be re.pattern)
        replace (str): replacement str
        source  (str): input filename
        count (int): number of occurrences to replace
        dest (str):   destination filename, if not given, source will be over written.        
    """
    try:
    	fin = open(source, 'r')
    except:
        print("sed fuction error! ==>source file cannor be found!")
    num_replaced = count

    if dest:
        fout = open(dest, 'w')
    else:
        fd, name = mkstemp()
        fout = open(name, 'w')

    for line in fin:
        out = re.sub(pattern, replace, line)
        fout.write(out)

        if out != line:
            num_replaced += 1
        if count and num_replaced > count:
            break
    try:
        fout.writelines(fin.readlines())
    except Exception as E:
        raise E

    fin.close()
    fout.close()

    if not dest:
        shutil.move(name, source) 

############################################################################################
solver="solids4Foam"        
OrigCase="baseCase"
Folder = "ml_dataset/train"
Folder2 = "ml_dataset/test"
mkpath(Folder)
 
curpath=os.path.abspath('.')

def randomSample():
    endPoint = [] 
    endPoint.append(round(np.random.uniform(1.0,2.0),5)) 
    endPoint.append(round(np.random.uniform(0.2,0.4*endPoint[0]),5)) 
    tractionL =	[ round(np.random.uniform(1.0,1.001),1), round(np.random.uniform(1,1.001),1) ]

    tractionB =	[ round(np.random.uniform(1.0,1.001),1), round(np.random.uniform(1.0,1.001),1) ]
    
    

    return endPoint,tractionL,tractionB

train_samples =300 
test_samples = 500

points = []
tractionsL =[]
tractionsB =[]
data_train = []
data_test = []
for i in range(0,train_samples):
    p,tl,tb = randomSample()
    datai=(p,tl,tb, Folder)
    points.append(p)
    tractionsL.append(tl)
    tractionsB.append(tb)
    data_train.append(datai)

for i in range(0,test_samples):
    p,tl,tb = randomSample()
    datai=(p,tl,tb,Folder2)
    points.append(p)
    tractionsL.append(tl)
    tractionsB.append(tb)
    data_test.append(datai)
#cells=[[20,5],[40,10],[80,20],[160,40]]
############################################################################################

#---------------------defining the function for cloning and running--------------------

def setupAndRunCase(geo,loadL,loadB, pathData):
    
    #-------Creating new case directory----------
    name=str(geo[0])+'_'+str(geo[1])+'-'+str(loadL[0])+'_'+str(loadL[1])
    rLRatio = geo[1]/(geo[0]-2*geo[1])
    
    totalEdge = 12
    cellsX=round(totalEdge*rLRatio/(1+rLRatio))#160
    cellsY=totalEdge-cellsX#20#int(cellsX*geo[1]/geo[0])

    cellsX_crt=20#20#int(cellsX*geo[1]/geo[0])
    cellsY_crt=20#20#int(cellsX*geo[1]/geo[0])
    
    NewCase=os.path.join(pathData,name)

    try:
    	shutil.copytree(OrigCase,NewCase)
    except:
    	return NewCase
    
    
    # replace the blockMesh file with correct values
    filePath=path.join(NewCase, 'constant/polyMesh','blockMeshDict')
    filePath2=path.join(NewCase, '0','D')


    thispath=os.path.abspath('.')
    sed('cellsX',str(cellsX), filePath)
    sed('cellsY',str(cellsY), filePath)
    sed('length',str(geo[0]), filePath)
    sed('radius1',str(geo[1]), filePath)
    sed('radius2',str(geo[1]*2), filePath)

    sed('P1x',str(geo[1]*0.9), filePath)
    sed('P1y',str(geo[1]*np.math.sqrt(1.0-0.81)), filePath)

    sed('P2x',str(2.0*geo[1]*0.9), filePath)
    sed('P2y',str(2.0*geo[1]*np.math.sqrt(1.0-0.81)), filePath)



    sed('r1x',str(np.math.sqrt(2.0)*geo[1]/2.0), filePath)
    sed('r2x',str(np.math.sqrt(2.0)*geo[1]), filePath)
    sed('PressHole',str(loadL[0]), filePath2)

    sed('jSize',str(cellsX_crt),path.join(NewCase, 'Allrun'))
    sed('iSize',str(cellsY_crt),path.join(NewCase, 'Allrun'))
   
    # preprocess = PrepareCase(args=NewCase+' --case-setup-script setupCase.sh')
    start_dir = os.path.abspath(os.path.curdir)
    
    os.chdir(NewCase)
    p0 = subprocess.Popen('bash Allclean',shell=True)
    p0.wait()
    p1 = subprocess.Popen('bash Allrun',shell=True)
    p1.wait()

    #p2 = subprocess.Popen('dataGenerate')
    #p2.wait()
    os.chdir(start_dir)
    
    return NewCase
#------------------------Running the Function in parallel-----------------------


with mp.Pool(processes=4) as pool:
    pool.starmap(setupAndRunCase, data_train)
    pool.starmap(setupAndRunCase, data_test)
   



