#!/bin/bash
mkdir images
i=0
for FOLDER in model_test_output/*
do
    base=${FOLDER##*/}
    touch $FOLDER/case.foam
    i=$((i+1))
    echo $i " : "  $base
    pvpython imageCreation.py $FOLDER $base 'F' 'cartesianMesh/internalMesh'
    pvpython imageCreation.py $FOLDER $base 'mask' 'cartesianMesh/internalMesh'
    pvpython imageCreation.py $FOLDER $base '' 'internalMesh'
    cp model_test_output/$base/$base'.png' images
done
