import multiprocessing as mp
import sys,os
import numpy as np
from os import path
import subprocess
import itertools
import re
import shutil
from tempfile import mkstemp
from distutils.dir_util import mkpath



def sed(pattern, replace, source, dest=None, count=0):
    """Reads a source file and writes the destination file.

    In each line, replaces pattern with replace.

    Args:
        pattern (str): pattern to match (can be re.pattern)
        replace (str): replacement str
        source  (str): input filename
        count (int): number of occurrences to replace
        dest (str):   destination filename, if not given, source will be over written.        
    """
    try:
    	fin = open(source, 'r')
    except:
        print("sed fuction error! ==>source file cannor be found!")
    num_replaced = count

    if dest:
        fout = open(dest, 'w')
    else:
        fd, name = mkstemp()
        fout = open(name, 'w')

    for line in fin:
        out = re.sub(pattern, replace, line)
        fout.write(out)

        if out != line:
            num_replaced += 1
        if count and num_replaced > count:
            break
    try:
        fout.writelines(fin.readlines())
    except Exception as E:
        raise E

    fin.close()
    fout.close()

    if not dest:
        shutil.move(name, source) 

############################################################################################
solver="solids4Foam"        
OrigCase="baseCase"
Folder = "ml_dataset/train"
Folder2 = "ml_dataset/test"
mkpath(Folder)
mkpath(Folder2)
 
curpath=os.path.abspath('.')

def randomSample():
    endPoint = [] 
    endPoint.append(round(np.random.uniform(1.0,1.00000001),5)) #0 Lt 
    endPoint.append(round(np.random.uniform(0.3,0.7*endPoint[0]),5)) #1 L1 
    endPoint.append(round(np.random.uniform(.99999999,1.0),5)*endPoint[0]) #2 Ht 
    endPoint.append(round(np.random.uniform(0.3,0.7*endPoint[2]),5)) #3 H1
    endPoint.append(round(np.random.uniform(0.85,0.95),5)*endPoint[3]) #4 Y1I # endPoint[3]*(1-0.33**2)**0.5 
    endPoint.append(round(np.random.uniform(0.7,0.9),5)*endPoint[4]) #5 Y2I
    endPoint.append(round(np.random.uniform(0.85,0.95),5)*endPoint[2]) #6 Y1O  
    endPoint.append(round(np.random.uniform(0.7,0.9),5)*endPoint[6]) #7 Y2O   
    tractionL =	[round(np.random.uniform(1.0,1.01),1)] # load

    
    

    return endPoint,tractionL

train_samples =300 
test_samples = 500

points = []
tractionsL =[]
data_train = []
data_test = []
for i in range(0,train_samples):
    p,tl = randomSample()
    datai=(p,tl,Folder)
    points.append(p)
    tractionsL.append(tl)
    data_train.append(datai)

for i in range(0,test_samples):
    p,tl = randomSample()
    datai=(p,tl,Folder2)
    points.append(p)
    tractionsL.append(tl)
    data_test.append(datai)

############################################################################################

#---------------------defining the function for cloning and running--------------------

def setupAndRunCase(geo,loadL,pathData):
    
    #-------Creating new case directory----------
    name=str(geo[0])+'_'+str(geo[1])+'-'+str(loadL[0])
    rLRatio = geo[1]/(geo[0]-2*geo[1])
    

    cellsX=15
    cellsY=6

    cellsX_crt=20
    cellsY_crt=20
    NewCase=os.path.join(pathData,name)

    try:
    	shutil.copytree(OrigCase,NewCase)
    except:
    	return NewCase
	
    
    
    # replace the blockMesh file with correct values
    filePath=path.join(NewCase, 'constant/polyMesh','blockMeshDict')
    filePath2=path.join(NewCase, '0','D')


    thispath=os.path.abspath('.')
    sed('cellsX',str(cellsX), filePath)
    sed('cellsY',str(cellsY), filePath)
    sed('Lt',str(geo[0]), filePath)
    sed('L1',str(geo[1]), filePath)
    sed('Ht',str(geo[2]), filePath)
    sed('H1',str(geo[3]), filePath)
    sed('XI',str(geo[1]*0.33), filePath)
    sed('XO',str(geo[0]*0.33), filePath)
    sed('YI',str(geo[4]), filePath)
    sed('YO',str(geo[6]), filePath)
    sed('load',str(loadL[0]), filePath2)
    sed('jSize',str(cellsX_crt),path.join(NewCase, 'Allrun'))
    sed('iSize',str(cellsY_crt),path.join(NewCase, 'Allrun'))
   
    # preprocess = PrepareCase(args=NewCase+' --case-setup-script setupCase.sh')
    start_dir = os.path.abspath(os.path.curdir)
    
    os.chdir(NewCase)
    p0 = subprocess.Popen('bash Allclean',shell=True)
    p0.wait()
    
    p1 = subprocess.Popen('bash Allrun',shell=True)
    p1.wait()

    os.chdir(start_dir)
    
    return NewCase
#------------------------Running the Function in parallel-----------------------


with mp.Pool(processes=4) as pool:
    pool.starmap(setupAndRunCase, data_train)
    pool.starmap(setupAndRunCase, data_test)









