import multiprocessing as mp
import sys,os
import numpy as np
from os import path
import subprocess
import itertools
import re
import shutil
from tempfile import mkstemp
from distutils.dir_util import mkpath



def sed(pattern, replace, source, dest=None, count=0):
    """Reads a source file and writes the destination file.

    In each line, replaces pattern with replace.

    Args:
        pattern (str): pattern to match (can be re.pattern)
        replace (str): replacement str
        source  (str): input filename
        count (int): number of occurrences to replace
        dest (str):   destination filename, if not given, source will be over written.        
    """
    try:
    	fin = open(source, 'r')
    except:
        print("sed fuction error! ==>source file cannor be found!")
    num_replaced = count

    if dest:
        fout = open(dest, 'w')
    else:
        fd, name = mkstemp()
        fout = open(name, 'w')

    for line in fin:
        out = re.sub(pattern, replace, line)
        fout.write(out)

        if out != line:
            num_replaced += 1
        if count and num_replaced > count:
            break
    try:
        fout.writelines(fin.readlines())
    except Exception as E:
        raise E

    fin.close()
    fout.close()

    if not dest:
        shutil.move(name, source) 

############################################################################################
solver="solids4Foam"        
OrigCase="baseCase"
Folder = "ml_dataset/train"
Folder2 = "ml_dataset/test"
mkpath(Folder)
mkpath(Folder2)
 
curpath=os.path.abspath('.')

def randomSample():
    endPoint =	[ round(np.random.uniform(0.5,1.0),3), round(np.random.uniform(0.05,0.1),3) ]

    traction =	[ round(np.random.uniform(0.0,0.01),1), round(np.random.uniform(1.0,1.01),1) ]

    
    

    return endPoint,traction

train_samples =300 
test_samples = 500

points = []
tractionsL =[]
data_train = []
data_test = []
for i in range(0,train_samples):
    p,tl = randomSample()
    datai=(p,tl,Folder)
    points.append(p)
    tractionsL.append(tl)
    data_train.append(datai)

for i in range(0,test_samples):
    p,tl = randomSample()
    datai=(p,tl,Folder2)
    points.append(p)
    tractionsL.append(tl)
    data_test.append(datai)

############################################################################################

#---------------------defining the function for cloning and running--------------------

def setupAndRunCase(geo,load,pathData):
    
    #-------Creating new case directory----------
    name=str(geo[0])+'_'+str(geo[1])+'-'+str(load[0])
    rLRatio = geo[1]/(geo[0]-2*geo[1])
    

    cellsX=50
    cellsY=8

    cellsX_crt=20
    cellsY_crt=20
    NewCase=os.path.join(pathData,name)

    try:
    	shutil.copytree(OrigCase,NewCase)
    except:
    	return NewCase
	
    
    
    # replace the blockMesh file with correct values
    filePath=path.join(NewCase, 'constant/polyMesh','blockMeshDict')
    filePath2=path.join(NewCase, '0','D')


    thispath=os.path.abspath('.')
    sed('cellsX',str(cellsX), filePath)
    sed('cellsY',str(cellsY), filePath)
    sed('length',str(geo[0]), filePath)
    sed('height',str(geo[1]), filePath)
    sed('loadX',str(load[0]), filePath2)
    sed('loadY',str(load[1]), filePath2)
    sed('jSize',str(cellsX_crt),path.join(NewCase, 'Allrun'))
    sed('iSize',str(cellsY_crt),path.join(NewCase, 'Allrun'))

    # preprocess = PrepareCase(args=NewCase+' --case-setup-script setupCase.sh')
    start_dir = os.path.abspath(os.path.curdir)
    
    os.chdir(NewCase)
    p0 = subprocess.Popen('bash Allclean',shell=True)
    p0.wait()
    
    p1 = subprocess.Popen('bash Allrun',shell=True)
    p1.wait()

    os.chdir(start_dir)
    
    return NewCase
#------------------------Running the Function in parallel-----------------------


with mp.Pool(processes=4) as pool:
    pool.starmap(setupAndRunCase, data_train)
    pool.starmap(setupAndRunCase, data_test)









