# Machine Learning classes and Utilities #

The contents of this project consist of modules (including libraries and utilities) and tutorials to showcase how TensorFlow C API can be incorporated into a new ML-based class.


### What is this repository for? ###
The focus of this project is to generate OpenFOAM fields through a pre-trained ML model and OpenFOAM variables (as inputs) in the OpenFOAM solvers. To simplify the application,
and follow OpenFOAM approach, the project is provided in three parts: src, utilities, and tutorial.


src:

The libraries consist of two main classes: machineLearningModel, and machineLearningInitializer.
The purpose of these two classes is to create general and specific classes, respectively, in OpenFOAM that can make predictions from the trained machine learning model.
In such classes, the following tasks are considered:

a) A function to exchange the information between TensorFlow tensors and OF fields.

b) Load frozen model (*.pb)

c) Create required inputs of the model from the case under consideration

c-1) Create a cartesian mesh from the case

c-2) Create an meshToMeshInterpolation object to transfer the BCs and mask to cartesian mesh

c-3) Convert coordinate of the cells and mesk and BCs to a TensorFlow tensor using (a)

d) predict (generate) solution field

Note1: In order to allow for generalizing the ML application in OF, first, we create a class of machineLearningModel from which other particular classes,
e.g. machineLearningInitializer, can be derived. The tasks (a), (b), and (d) are more general, so are handled inside the machineLearningModel while the task
(c) is special for "linear operation technique with dual mesh idea" and is handled inside machineLearningInitializer.

Note2:
machineLearningModel has functions to convert a Foam::PtrList<scalarField> into tensorflow::tensor and vice versa which are the core components for transferring data
between OF and TF. To give some taste about these functions, the scalarFields (A, B and C) in the PtrList are concatenated to create a tensorflow tensor as below:

<A B C> ⇒ [ a1 b1 c1; a2 b2 c2; a3 b3 c3; ....]


where ai, bi and ci are the corresponding components of A, B and C, respectively. More simply, the OF fields are organized as a 1-D tensor which then are concatenated
on a new axis as the second dimension.
It should be noted that LHS is an OF variable while RHS is a tensorflow variable.

Note3: Since the ML model might have several inputs, this class needs the name of input and output layers as std::vector<std::string> and also all of inputs as
std::vector<Foam::PtrList<scalarField>>. Each component of the latter vector must correspond to each component of the input layer names, respectively.

MachinLearningInitializer
This class is a special case for the machineLearningModel class, so it is derived from it. MachinLearningInitializer has a field which is an object from linOpCoeffGenerator class. linOpCoeffGenerator class is mainly used to generate gradient and divergence coefficients from a mesh and boundary conditions in an OF case.
Here, the predict function is overvided because the architecture for this network is known (It has 7 inputs and 1 outputs).
This class is still under development.

 
utility: (In progress)

These are two simple utilities to demonstrate how to utilize these classes. MLSurrogate actually acts inside a case and generates a solution field for that case using cnn.pb (pre-trained
model) located inside the case. The MLInit does the same but generates a solution field for a number of cases at the same time.


### How do I get set up? ###

Use a dockerfile as mentioned in the root of this repository and run:

cd smartsim && ./Allwmake


 






