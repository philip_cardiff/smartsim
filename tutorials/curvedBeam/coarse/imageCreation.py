#### import the simple module from the paraview
import sys
from paraview.simple import *
#from paraview import servermanager

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()
case_loc = sys.argv[1] + '/case.foam'
image_id = sys.argv[2]
var_contour = sys.argv[3]
region=sys.argv[4]

casefoam = OpenFOAMReader(FileName=case_loc)
casefoam.MeshRegions = [region]
casefoam.CellArrays = [var_contour]


# get animation scene
#animationScene1 = GetAnimationScene()




# Properties modified on a160OpenFOAM
#casefoam.VolumeFields = ['D']
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [2184, 1162]
renderView1.Background = [1.0, 1.0, 1.0]


# get display properties
#casefoamDisplay = GetDisplayProperties(casefoam, view=renderView1)
casefoamDisplay = Show(casefoam, renderView1)
renderView1.ResetCamera()
renderView1.Update()
# set scalar coloring
ColorBy(casefoamDisplay, ('CELLS', var_contour))

# rescale color and/or opacity maps used to include current data range
casefoamDisplay.RescaleTransferFunctionToDataRange(True)

# show color bar/color legend
casefoamDisplay.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'D'
dLUT = GetColorTransferFunction('D')

# get opacity transfer function/opacity map for 'D'
dPWF = GetOpacityTransferFunction('D')



# get animation scene
animationScene1 = GetAnimationScene()

animationScene1.GoToLast()

# rescale color and/or opacity maps used to exactly fit the current data range
casefoamDisplay.RescaleTransferFunctionToDataRange(False)
casefoamDisplay.SetRepresentationType('Surface With Edges')
# current camera placement for renderView1
#renderView1.CameraPosition = [1.0, 0.05000000074505806, 3.8787228587726825]
#renderView1.CameraFocalPoint = [1.0, 0.05000000074505806, 0.009999999776482582]
#renderView1.CameraParallelScale = 1.0012991561316904
renderView1.ResetCamera()

# save screenshot
#SaveScreenshot('/home/emad/Documents/UCD/work/iteration_acceleration/2D_funky_compression/model_test_output/160/case.png', magnification=1, quality=100, view=renderView1)
#### saving camera placements for all active views
# save screenshot
from distutils.dir_util import mkpath

SaveScreenshot(sys.argv[1] + '/' + var_contour + image_id + '.png', magnification=1, quality=100, view=renderView1)


# current camera placement for renderView1
#renderView1.CameraPosition = [1.0, 0.05000000074505806, 2.31262509245734]
#renderView1.CameraFocalPoint = [1.0, 0.05000000074505806, -1.5560977665388598]
#renderView1.CameraParallelScale = 1.0012991561316904

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
