# What is the purpose of these tutorials?

These tutorials demonstrate how to:

1- Generate dataset from an OpenFOAM case;
2- Create and train ML networks (supervised and physics-informed);
3- Compare the prediction of ML model with OpenFOAM results;
4- Load the predictive model into OpenFOAM solvers as a surrogate or initializer.



# What are the requirements for these tutorials?

The Docker image includes all requirements and dependencies to run these tutorials.


# How do I run the tutorials?

From within the Docker container, execute the Allrun script:

    ./Allrun

This Allrun script will execute the Allrun script for the curvedBeam tutorial. The Allrun script performs three actions:

1- Data generation from an OpenFOAM base case;
2- Training a machine learning model based on this data;
3- Accelerating the OpenFOAM solids4foam solver via the trained machine learning surrogate/emulator model.

These three steps are described in more details below:

## Data generation from an OpenFOAM base case

For generating data from the included OpenFOAM base/template case, the following command is run:

    python3 dataCollection.py

The `dataCollection.py` python script generates and runs random configurations of the OpenFOAM base/template case for a variety of geometry and loading parameter ranges. Once this command completes, a new directory `ml_dataset` will have been created containing training and test data suitable for machine learning models.

#### Philip: why not add this ulimit command to the Allrun script and we can remove this from here
#### Emad: I did in line 13 of the python script (i.e. dataCollection.py), but still sometimes the script crashses if we increase samples to more than 500.
Note: Due to limitation of the resource usage in the linux, you might need to run this command several times in order to generate enough samples within the dataset. (check "ulimit" command in linux)


## Training a machine learning model based on this data

For the training, run the following command:

    python3 train_ml_model.py

This python script generates a "predictive_sup.pb" or "predictive_uns.pb" file (depending on the parameters in the train_ml_model.py), which contains a trained machine learning model; this will be used for predictions in step 3.

Notes:
- Instead of using this command, you can open a jupyter notebook and load "train_ml_model.ipynb" within the notebook; you can change from supervised to unsupervised, change the number of training data, architecture of the network, etc.
- For exporting the supervised model, inverse normalisation of the output is implemented before saving the model. So in C++, the output of the loaded supervised model is the actual (not normalized) displacement field.


## Accelerating the OpenFOAM solids4foam solver via the trained machine learning surrogate/emulator model
After training the machine learning model, go to the 'ml_dataset/test' path. In the 'constant' directory, a new dictionary called 'machineLearningProperties' is used to configure the name of the predictive ML model as well as its inputs and outputs.

#### Emad: There are many unseen cases within the 'ml_dataset/test' path. So, we need to use a loop. I added the loop in tutorials/Allrun
To use the machine learning model as a surrogate, we need to put '-1' for 'nCorrectors' in the 'solidProperties' dictionary and run the solids4foam. In that case, the solids4foam solver does not calculate anything and instead just calls the machine learning model. The solver can be run as:

    (cd ml_dataset/test/[caseName] && soldis4Foam)

To use the machine learning model as an initialiser for the solids4foam solution, set 'predictor' to 'true' in the 'solidProperties' dictionary.

Note: The 'Allrun' script applies ML predictions for all cases within test samples and evaluate maximum, minumum and average of the speedup given by ML initializer.
