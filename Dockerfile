################################################################################
#
# SmartSim
#   A toolbox for machine learning with OpenFOAM
#
# Dockerfile to build the smartSim image for performing machine learning
# predictions using OpenFOAM via the solids4foam toolbox.
# This image starts from Ubuntu-18.04 and then adds foam-extend-4.0,
# solids4foam, TensorFlow and related python packages. TensorFlow is built from
# source using bazel to allow linking with OpenFOAM utilities and executables.
#
# Authors
#   Emad Tandis, UCD. emad.tandis@ucd.ie
#   Philip Cardiff, UCD. philip.cardiff@ucd.ie
#
# License
#   GPLv3, https://www.gnu.org/licenses/gpl-3.0.en.html
#
################################################################################

# Start with UBUNTU 18.04
FROM ubuntu:18.04
MAINTAINER emadUCD<emad.tandis@ucd.ie>

# Install general softwares
RUN apt-get update
RUN apt install -y vim
RUN apt-get install -y git

# Install python and keras
RUN apt-get update
RUN apt install -y python3-dev python3-pip
RUN pip3 install  -U --user pip six 'numpy<1.19.0' wheel setuptools mock 'future>=0.17.1' 'gast==0.3.3' typing_extensions
RUN pip3 install  -U --user keras_applications --no-deps
RUN pip3 install  -U --user keras_preprocessing --no-deps
RUN apt-get update
RUN apt-get install -y python
RUN pip3 install pandas
RUN pip3 install sklearn
RUN pip3 install matplotlib
RUN pip3 install Ipython

# Install bazel and protobuf
RUN apt install -y curl gnupg
RUN curl -fsSL https://bazel.build/bazel-release.pub.gpg | gpg --dearmor > bazel.gpg
RUN mv bazel.gpg /etc/apt/trusted.gpg.d/
RUN echo "deb [arch=amd64] https://storage.googleapis.com/bazel-apt stable jdk1.8" | tee /etc/apt/sources.list.d/bazel.list 
RUN apt install -y protobuf-c-compiler
RUN apt update
RUN apt install -y bazel


# Download tensorflow and build c++ libraries
# This may take 1-2 hours depending on your system
# There must be enough RAM for this part, at least than 8.0 GB
RUN git clone https://github.com/tensorflow/tensorflow.git
RUN chmod +x ./tensorflow/configure
RUN chmod +x ./tensorflow/tensorflow/tools/ci_build/install/install_bazel.sh && ./tensorflow/tensorflow/tools/ci_build/install/install_bazel.sh
RUN echo "n n n n n n n n n n n n" | ./tensorflow/configure


RUN mkdir /bazel_build

RUN cd ./tensorflow && bazel --output_base=/bazel_build build -c opt --config=v2 //tensorflow:libtensorflow.so
RUN cd ./tensorflow && bazel --output_base=/bazel_build build -c opt --config=v2 //tensorflow:libtensorflow_cc.so
RUN cd ./tensorflow && bazel --output_base=/bazel_build build -c opt --config=v2 //tensorflow/tools/pip_package:build_pip_package
RUN mkdir /tensorflow/bazel-bin/tensorflow/include
RUN cp -a /tensorflow/bazel-bin/tensorflow/tools/pip_package/build_pip_package.runfiles/. /tensorflow/bazel-bin/tensorflow/include/
RUN echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/tensorflow/bazel-bin/tensorflow/" >> /root/.bashrc



# Download and install OpenFOAM
# Depending on your system, it takes 1-2 hours
RUN apt-get install -y git-core build-essential binutils-dev cmake flex bear zlib1g-dev libncurses5-dev libreadline-dev libxt-dev rpm mercurial graphviz python python-dev  python3 python3-dev gcc-5 g++-5 gnuplot gnuplot-qt gnuplot-data
RUN cd opt && mkdir foam && cd foam && git clone git://git.code.sf.net/p/foam-extend/foam-extend-4.0 foam-extend-4.0
RUN cd opt/foam/foam-extend-4.0 && echo "export WM_THIRD_PARTY_USE_BISON_27=1"  >> etc/prefs.sh && echo "export WM_CC='gcc-5'"  >> etc/prefs.sh && echo "export WM_CXX='g++-5'"  >> etc/prefs.sh && echo "export QT_BIN_DIR='/user/bin/'"  >> etc/prefs.sh
RUN cd opt/foam/foam-extend-4.0 && echo "export QT_BIN_DIR='/usr/bin/'" >> etc/prefs.sh
RUN apt install -y mpi
RUN echo "source opt/foam/foam-extend-4.0/etc/bashrc"  >> /root/.bashrc
RUN echo "source opt/foam/foam-extend-4.0/etc/bashrc" >> ~/.bashrc

# Hack OpenFOAM source code
RUN sed -i '0,/public:/s|public:|        //Get cell addressing\n        const labelList\&\ cellAddress() const\n        {\n            return cellAddressing_;\n        }|g' /opt/foam/foam-extend-4.0/src/sampling/meshToMeshInterpolation/meshToMesh/meshToMesh.H
RUN sed -i "s|//- Return inverse distance weights|public:\n        //- Return inverse distance weights|g" /opt/foam/foam-extend-4.0/src/sampling/meshToMeshInterpolation/meshToMesh/meshToMesh.H

RUN sed -i 's|$HOME|/opt|g' /opt/foam/foam-extend-4.0/etc/bashrc
#RUN sed -i "s|# foamInstall=/opt/$WM_PROJECT|foamInstall=/opt/$WM_PROJECT|g" /opt/foam/foam-extend-4.0/etc/bashrc


# Compile foam extend
RUN /bin/bash -c ". /opt/foam/foam-extend-4.0/etc/bashrc && cd /opt/foam/foam-extend-4.0 && ./Allwmake.firstInstall"


# Download and install solids4foam toolbox
RUN apt update
RUN cd /opt/foam && git clone https://bitbucket.org/philip_cardiff/solids4foam-release.git

ENV solids4Foam=/opt/foam/solids4foam-release
ENV solids4Foam_SRC=/opt/foam/solids4foam-release/src/
RUN cp /opt/foam/solids4foam-release/filesToReplaceInOF/findRefCell.C /opt/foam/foam-extend-4.0/src/finiteVolume/cfdTools/general/findRefCell/

# Compile solids4foam and re-compile foam extend libraries which were changed
RUN /bin/bash -c ". /opt/foam/foam-extend-4.0/etc/bashrc && wmake libso /opt/foam/foam-extend-4.0/src/finiteVolume && cd /opt/foam/solids4foam-release && ./Allwmake"

# Final changes for tensorflow
RUN chmod +x tensorflow/bazel-bin/tensorflow/tools/pip_package/build_pip_package
RUN /bin/bash -c "cd tensorflow && . bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg && pip3 install /tmp/tensorflow_pkg/tensorflow*.whl"



# Add smartsim code to image
RUN mkdir /smartsim
RUN mkdir /smartsim/src
RUN mkdir /smartsim/tutorials
COPY src /smartsim/src


RUN apt install bc
ENV mlSrc=/smartsim/src/foamClasses4TensorFlowPredictions/src/
RUN /bin/bash -c "cd /smartsim/src  && ./Allwmake"

COPY tutorials /smartsim/tutorials


