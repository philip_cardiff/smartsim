/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | foam-extend: Open Source CFD
   \\    /   O peration     | Version:     4.0
    \\  /    A nd           | Web:         http://www.foam-extend.org
     \\/     M anipulation  | For copyright notice see file Copyright
-------------------------------------------------------------------------------
License
    This file is part of foam-extend.

    foam-extend is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    foam-extend is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with foam-extend.  If not, see <http://www.gnu.org/licenses/>.

Application
    meshDataGenerator

Description
   A utility to generate and write foam data which can be used to calculate grad and div in Python.

\*---------------------------------------------------------------------------*/
#ifndef linOpCoeffGenerator_H
#define linOpCoeffGenerator_H

// Including C++ libraies
# include "fvCFD.H"
#include "timeSelector.H"
//#include "blockSolidTractionFvPatchVectorField.H"
#include "solidTractionFvPatchVectorField.H"
#include "OSspecific.H"
#include <sys/types.h> 
#include <sys/stat.h>


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

using namespace Foam;
//namespace Foam
//{


/*---------------------------------------------------------------------------*\
        Class machineLearningInitializer Declaration
\*---------------------------------------------------------------------------*/


class linOpCoeffGenerator
{
    // Private data
    PtrList<scalarField> fixedBc_;
    PtrList<scalarField> tracBc_;
    PtrList<scalarField> cellCoeff_;
    PtrList<scalarField> cellAddress_; // Change label to scalar for the sake of consistency with other inputs of the network. 

// Private Member Functions

        //- Disallow default bitwise copy construct
    


public:

    //- Runtime type information
    //TypeName("machineLearningInitializer");


    // Constructors

        //- Construct from file name
	linOpCoeffGenerator(volVectorField& field, int nCells, int nFix, int nTrac);
	


    // Destructor

        


    // Member Functions
    void createIntCellsData(const fvMesh& mesh, int nCell);

    void createBcData(volVectorField& D,int nFix, int nTrac);
 
    void writeLinCoeffsData(volVectorField& D);


    PtrList<scalarField>& cellCoeffs()
    {
	return cellCoeff_;
    }

    PtrList<scalarField>& cellAddress() // Change label to scalar for the sake of consistency with other inputs of the network. 
    {
	return cellAddress_;
    }

    PtrList<scalarField>& tracBc()
    {
	return tracBc_;
    }

    PtrList<scalarField>& fixedBc()
    {
	return fixedBc_;
    }
        
        
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //


// ************************************************************************* //
